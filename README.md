# Einfache Web-App in einen Container  verfrachten 

## Image ONE
Dies ist eine einfach zusammengestrickte node.js-Anwendung, um den Einsatz von Microservices in Containern aufzuzeigen

Dieses Image wird **nicht** gepflegt und beinhaltet allfällige "Vulnerabilities". <br> 
Nutzung auf eigenes Risiko**

Der Web-Dienst wird auf Port `8080` freigegeben - siehe `./app.js`

Weitere Details sind im `Dockerfile` ersichtlich

### Wie kann diese einfache Web-App erstellt, angepasst und genutzt werden:
- Dieses Repository clonen
- Content anpassen
- Docker-Image erstellen und in die Registry pushen
- Docker-Image laufen lassen (Container)
- Auf den laufenden Container zugreifen
- Container und Image anschauen / inspizieren
- Container und Image manipulieren (stoppen, starten, löschen)

- [Atom](https://atom.io/) oder [Sublime Text](https://www.sublimetext.com/) etc...
---

### Ausgangslage
Um ein neues Container-Image zu erstellen, muss Docker installiert sein. Für dieses Hands-on wird eine VM aus der TBZ-Cloud eingesetzt (Setup M300). Weitere Bedingungen sind in diesem Fall ein Host (Laptop) mit Gitbash, einer VPN-Verbindung zur TBZ-Cloud (Wireguard) und eine dedizierte VM. In diesem Fall handelt es sich um die VM mit der IP-Adresse 10.4.43.32. 

Per SSH auf die VM "hüpfen" :  
```
$ ssh ubuntu@10.4.43.32
```
### Vorbereitung

Zuerst im Home-Verzeichnis ein Unterverzeichnis `TEMP_Docker` erstellen und reinhüpfen 

> `$ mkdir TEMP_Docker ` _Unterverzeichnis "TEMP_Docker" erstellen_<br>
> `$ cd TEMP_Docker  ` _Ins Unterverzeichnis "TEMP_Docker" wechseln_ <br>
> `$ git clone https://gitlab.com/ser-cal/Container-CAL-webapp_v1.git  ` _Repo klonen_<br>
> `$ cd Container-CAL-webapp-v1/ ` _ins Repo-Unterverzeichnis hüpfen_<br>
> `$ cd APP  ` _Ins Unterverzeichnis "APP" hüpfen_ <br>
> `$ less Dockerfile ` _Inhalt des Dockerfiles anschauen_<br>
> `$ docker --version ` _Nochmals sicherstellen, dass Docker installiert ist (Notwendig)_<br>
  ![Screenshot](images/1.png) 


---

Im Verzeichnis **`APP`** gibt es folgende Files:
- **`views  `** _Verzeichnis mit **`home.pug`**-File (Content Webseite)_<br>
  _Im **`home.pug`**-File kann der Inhalt der Webseite geändert werden_<br>
- `app.js  ` _Node-js-App - App-config (Port etc..)_
- `bootstrap.css  ` _Style_
- `Dockerfile  ` _Image-Layers (OS, ergänzende SW-Deployment + Portweiterleitung)_
- `package.json  ` _Manifest_


Das sind sämtliche Files, die es für diese einfache Web-App benötigt. 

### Content erstellen / anpassen
Es können nun in den oben aufgeführten Files Anpassungen oder Änderungen durchgeführt werden.

Wenn diese Einträge und das Coden des Web-contents abgeschlossen sind, kann daraus ein neues Docker-Image erstellt werden. Dieser Schritt wird wie folgt durchgeführt: 

### Neues Docker-Image bauen

```
$ docker image build -t marcellocalisto/webapp_one:1.0 .
```

> `$ docker image build -t marcellocalisto/webapp_one:1.0 . ` _Erstelle ein neues Image in diesem Verzeichnis_<br>
  ![Screenshot](images/2b.png) 


Auf dem Screenshot ist ersichtlich, dass das Image erzeugt wird (6 Schritte, wie im Dockerfile vorgesehen. Die "intermediate container" werden temporär erstellt und verwendet, um einen dieser Schritte gem. Dockerfile umzusetzen (danach werden diese wieder gelöscht)

  * `marcellocalisto `_Docker-Account (Verwende hier Deinen eigenen Docker-Account)_
  * `webapp_one `_Image-Name_
  * `1.0 `_Image-Version_
  * `.    ` _Verzeichnis (in diesem Fall das aktuelle Verzeichnis)_


### Neues Docker-Image überprüfen

```
$ docker image ls
```

> `$ docker image ls ` _Überprüfen, ob Image vorhanden ist_<br>
  ![Screenshot](images/5b.png) 


Auf dem Screenshot sieht man zusätzlich auch noch folgendes:

  * `marcellocalisto/webapp_one `_1. Docker-Image (Repository-Name)_
  * `marcellocalisto/webapp_two `_2. Docker-Image (Repository-Name)_
  * `1.0 `_Tag_
  * `f3a0d6a9c988    ` _Image-ID_
  * `195MB    ` _Size_


### Neues Docker-Image hochladen auf Docker-Hub

```
$ docker image push marcellocalisto/webapp_one:1.0
```

> `$ docker image push marcellocalisto/webapp_one:1.0 ` _Image zu Dockerhub "pushen"_<br>
  ![Screenshot](images/3c.png) 


Auf dem Screenshot sieht man anschliessend eine Fehlermeldung (Zugriffsverweigerung). Dies geschieht, weil man sich noch mit seinem Docker-Account auf Dockerhub registrieren/einloggen muss.

#### Registrieren auf Docker-Hub
Mit dem entsprechenden Docker-Account einloggen und anschliessend nochmals das Docker-Image zum Repository "pushen". In diesem Fall benutze ich meinen eigenen Account. Ersetze meinen Namen (marcellocalisto) mit Deinem persönlichen Docker-Accountname.

```
$ docker login --username=marcellocalisto
$ docker image push marcellocalisto/webapp_one:1.0
```

> `$ docker login --username=marcellocalisto ` _Username + Passwort vom Docker-Account_<br>
> `$ docker image push marcellocalisto/webapp_one:1.0 ` _Image zu Dockerhub "pushen"_<br>
  ![Screenshot](images/4b.png) 

Auf dem Screenshot wird festgehalten, dass das Passwort **unverschlüsselt** im Verzeichnis `/home/ubuntu/.docker/config.json` abgelegt wird. <br>
Es ist jetzt allerdings auch ersichtlich, dass beim zweiten Versuch **keine** Fehlermeldung (Zugriffsverweigerung) mehr erscheint. Das Hochladen in die Registry (Dockerhub) hat diesmal funktioniert.


### Neues Docker-Image überprüfen / inspizieren
In diesem Abschnitt überprüfen wir, ob Images mit gewissen Namensgesbungen (in diesem Fall "Calisto") lokal abgelegt sind. Zusätzlich wird gezeigt, wie ein lokales Image inspiziert werden kann (sehr umfangreiche Informationen)

```
$ docker image ls | grep -i calisto
$ docker image inspect <ID>
```

> `$ docker image ls | grep -i calisto ` _Checken, ob Images mit dem Namen "Calisto" vorhanden sind_<br>
> `$ docker image inspect <ID> ` _Image-Details inspizieren_<br>
  ![Screenshot](images/8.png) 


Auf dem Screenshot sieht man, dass zwei Images vorhanden sind. Ich habe noch ein zweites Image mit einem leicht abgeänderten Inhalt erstellt. Zu einem späteren Zeitpunkt wird anhand dieser beiden Images ein "Rolling Upgrade" unter Kubernetes demonstriert. Wir fokussieren uns aber im Moment auf das Image "One".

### Docker Container starten
Nun überprüfen wir noch, ob aus diesem Image ein funktionierender Container erzeugt werden kann. In diesem Tutorial führen wir dazu einen imperativen Befehl mit div. Parametern aus. Jeder Teil des Kommandos wird unten ausführlich erklärt

```
$ docker container marcellocalisto/webapp_one:1.0
```

> `$ docker container run -d --name cal-web -p 8080:8080 marcellocalisto/webapp_one ` _klappt nicht, da Tag fehlt"_<br>
> `$ docker container run -d --name cal-web -p 8080:8080 marcellocalisto/webapp_one:1.0 ` _Funktioniert"_<br>
  ![Screenshot](images/6c.png) 


Auf dem Screenshot ist zu sehen, dass erst **der zweite Versuch** klappt. Beim starten eines Containers muss in diesem Fall auch der Tag angegeben werden (wurde auch so beim Erstellen des Images eingegeben)

Hier noch Angaben zu den jeweiligen Parametern:

  * `-d `_Im Detach-Modus starten_
  * `--name `_Name des Containers - frei wählbar. In diesem Fall **cal-web**_
  * `8080:8080 `_HostPort:ContainerPort - Forwarding des Ports_

### Webdienst überprüfen
Überprüfen, ob der Webdienst nach dem **starten** des Containers wirklich über Port 8080 erreichbar ist

```
IP-Adresse:8080
```

> `10.4.43.32:8080 ` _Auf Host Browser starten und IP:Port des Webdienstes (Container) eingeben_<br>
  ![Screenshot](images/10.png) 


### Container Manipulationen (starten / überprüfen / stoppen / löschen)
In diesem Abschnitt widmen wir uns weiteren Manipulationen von Containern. Auch wenn oben bereits dokumentiert, wird hier zwecks Verständlichkeit nochmals gezeigt, wie ein Container im erweiterten Kontext gestartet wird. Es besteht jederzeit die Möglichkeit, diesen zu stoppen und jederzeit wieder zu starten. Falls nicht mehr benötigt, kann er (auch temporär) gelöscht werden.

```
$ docker container ls -a | grep -i calisto
$ docker container run -d --name cal-web01 -p 8080:8080 marcellocalisto/webapp_one:1.0
```

> `$ docker container ls -a | grep -i calisto ` _Checken, ob Container mit dem Namen "Calisto" läuft_<br>
> `$ docker container run -d --name cal-web01 -p 8080:8080 marcellocalisto/webapp_one:1.0 ` _Container starten_<br>
> `$ docker container inspect 055 ` _Container mit der ID 055* inspizieren_<br>
  ![Screenshot](images/9.png) 


#### Container-Manipulationen mit der Container-ID

```
$ docker container stop <Container-ID>
$ docker container stop <Container-ID>
$ ocker container rm <Container-ID>
```

#### Container-Manipulationen mit dem Container-Namen

```
$ docker container stop <Container-Name>
$ docker container stop <Container-Name>
$ ocker container rm <Container-Name>
```

> `$ docker container ls -a | grep -i calisto ` _Checken, ob Container mit dem Namen "Calisto" läuft_<br>
> `$ docker container stop 055 ` _Container mit der ID 055* stoppen_<br>
> `$ docker container start 055 ` _Container mit der ID 055* starten_<br>
> `$ docker container stop cal-web01 ` _Container mit dem Containernamen "cal-web01 stoppen_<br>
> `$ docker container rm 055 ` _Container mit der ID 055* löschen_<br>
  ![Screenshot](images/11.png) 

  ### Webdienst überprüfen
Überprüfen, ob der Webdienst nach dem **löschen** des Containers wirklich nicht mehr über Port 8080 erreichbar ist

```
IP-Adresse:8080
```

> `10.4.43.32:8080 ` _Auf Host Browser starten und IP:Port des Webdienstes (Container) eingeben_<br>
  ![Screenshot](images/12.png) 

